<?php

namespace Database\Seeders;

use App\Models\Topic;
use App\Models\Ticket;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i<20; $i++)
        {
            Topic::factory()->has(
                Topic::factory()->count(mt_rand(3,15))->has(
                    Ticket::factory()->count(mt_rand(2,10)),
                    'ticket'
                ),
                'parent'
            )->create();
        }
    }
}
