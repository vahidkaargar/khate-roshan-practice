<?php


namespace App\Http\Packages;
use PHPHtmlParser\Dom;


class Digikala
{
    public $url = 'https://www.digikala.com/product/';
    public $dom = null;
    public $id = null;

    public function __construct()
    {
        $this->dom = new Dom;
    }

    public function grab()
    {
        // Load html content of webpage
        $this->dom->loadFromUrl($this->url.$this->id);

        // Check if product exists
        $exists = $this->dom->find('title')[0];
        if($exists->innerText == 'Page Not Found')
            return null;

        /*
         * Export data from elements
         */
        // Title
        $title = $this->dom->find('h1.c-product__title')[0];
        $data['title'] = $title->innerText;

        // Summary
        $summary = $this->dom->find('.c-content-expert__summary')[0];
        $data['summary'] = $summary->innerText;

        // Price
        $price = $this->dom->find('div.c-product__seller-price-pure')[0];
        $data['price'] = $price->innerText;

        // Image
        $image = $this->dom->find('img.js-gallery-img')[0];
        $data['image'] = $image->getAttribute('data-src');

        return $data;
    }
}
