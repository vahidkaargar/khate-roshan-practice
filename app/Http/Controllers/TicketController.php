<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    //
    public function index()
    {
        $topics = Topic::with(['topic.ticket'])->where('parent_id', '=',0)
            ->latest()->get()->toArray();
        return view('topics.index', ['data' => $topics]);
    }

    public function show($id)
    {
        $topics = Topic::with(['ticket'])->where('id', $id)->first()->toArray();
        return view('topics.show', $topics);
    }
}
