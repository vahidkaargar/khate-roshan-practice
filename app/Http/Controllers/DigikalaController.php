<?php

namespace App\Http\Controllers;

use App\Http\Packages\Digikala;
use Illuminate\Http\Request;

class DigikalaController extends Controller
{
    // Digikala grabber form
    public function index()
    {
        return view('digikala.index');
    }

    public function preview(Request $request)
    {
        $params = $request->all(['id']);
        $digi = new Digikala;
        $digi->id = $params['id'];
        return view('digikala.preview', ['digi' => $digi->grab()]);
    }
}
