<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    use HasFactory;

    public function parent()
    {
        return $this->hasOne(Topic::class, 'parent_id', 'id');
    }

    public function topic()
    {
        return $this->hasMany(Topic::class, 'parent_id', 'id');
    }

    public function ticket()
    {
        return $this->hasMany(Ticket::class, 'topic_id', 'id');
    }
}
