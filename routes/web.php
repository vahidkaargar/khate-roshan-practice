<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DigikalaController;
use App\Http\Controllers\TicketController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Digikala Routes
Route::get('/digikala', [DigikalaController::class, 'index']);
Route::get('/digikala/preview', [DigikalaController::class, 'preview']);

// Tickets
Route::get('/tickets', [TicketController::class, 'index']);
Route::get('/tickets/{id}', [TicketController::class, 'show']);




