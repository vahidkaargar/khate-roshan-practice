<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>@yield('title')</title>
    <style>
        a{
            text-decoration: none;
        }
        body{
            background-color: rgba(200,200,200,.3)
        }
        .ltr{
            direction: ltr !important;
        }
        .rtl{
            direction: rtl !important;
        }
        .font-size-sm{
            font-size: 14px;
        }
        .font-size-md{
            font-size: 16px;
        }
        .font-size-xl{
            font-size: 18px;
        }
    </style>
</head>
<body>
<div class="container p-2 p-md-4">
    @yield('content')
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>
