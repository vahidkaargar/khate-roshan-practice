@extends('app')

@section('title', 'Digikala Grabber')


@section('content')
    <div class="card p-2 p-md-4 mx-auto" style="max-width: 480px">
        <div class="row">
            <form action="/digikala/preview" onsubmit="return digiKalaValidate()">
                <div id="danger" class="alert alert-danger" style="display: none">
                    Validation <strong>`Digikala ID`</strong> problem
                </div>
                <div id="success" class="alert alert-success" style="display: none">
                    Success. Redirecting to preview page...
                </div>
                <div class="my-2">
                    <label class="form-label" for="digikala-id">Digikala ID</label>
                    <input type="text" name="id" class="form-control" id="digikala-id" placeholder="dkp-2361428">
                </div>
                <div class="my-2">
                    <button type="submit" class="btn btn-primary">Preview</button>
                </div>
            </form>
        </div>
    </div>
    <script>
        function digiKalaValidate() {
            // Get Digikala ID
            var id = document.getElementById('digikala-id').value;
            // Error message element
            var danger = document.getElementById('danger');
            // Success message element
            var success = document.getElementById('success');
            // Regex pattern
            var regex = /(dkp-)\d{5,9}/;

            // If validation succeed
            if(regex.test(id)) {
                success.style.display = 'block'
                danger.style.display = 'none'
                return true;
            }

            // If validation failed
            danger.style.display = 'block'
            return false;
        }
        function digiKalaTest(id) {
            return /(dkp-)\d{5,9}/.test(id);
        }
    </script>
@endsection
