@extends('app')

@section('title', $digi['title'] ?? '404, Page not found')


@section('content')
    @if(empty($digi))
        <div class="p-5 text-center">
            <div class="mb-5"><img src="https://mobile.digikala.com/static/files/ddd53e3d.png"></div>
            <h1 class="text-muted">404, Page not found</h1>
        </div>
    @else
        <div class="card shadow-sm rtl">
            <div class="row">
                <div class="col-md-4 text-center">
                    <img class="img-thumbnail m-md-3" src="{{$digi['image']}}">
                    <a href="#" class="btn btn-success btn-lg m-3 m-md-0">{{$digi['price'] }} تومان</a>
                </div>
                <div class="col-md-8">
                    <section class="m-1 m-md-3">
                        <h1 class="h5 mb-2">{!! $digi['title'] !!}</h1>
                        <article class="lead font-size-xl p-md-2">{!! $digi['summary'] !!}</article>
                    </section>
                </div>
            </div>
        </div>
    @endif
@endsection
