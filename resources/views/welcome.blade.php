@extends('app')

@section('title', 'Welcome')

@section('content')
    <div class="card p-2 p-md-4 mx-auto text-center" style="max-width: 480px">
        <h1 class="text-muted mb-5">Welcome</h1>
        <a class="btn btn-primary mt-3" href="/digikala">Digikala Grabber</a>
        <a class="btn btn-primary mt-2" href="/tickets">Tickets</a>
    </div>
@endsection
