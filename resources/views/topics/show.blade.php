@extends('app')

@section('title', 'Topics')


@section('content')
    <div class="mx-auto" style="max-width: 720px;">
        <div class="card text-center mb-5">
            <div class="card-header">
                <button class="btn btn-sm btn-secondary mb-3" onclick="window.history.back()">&lt; Back</button>
                <h5>{!! $name !!}</h5>
            </div>
        </div>
        @foreach($ticket as $item)
            <div class="card my-4">
                <div class="card-body">
                    <p class="card-text">
                        {!! $item['body'] !!}
                    </p>
                </div>
                <div class="card-footer text-muted text-center">
                    {{ date('j F, Y', strtotime($item['created_at'])) }}
                </div>
            </div>
        @endforeach
    </div>
@endsection
