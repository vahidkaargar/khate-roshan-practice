@extends('app')

@section('title', 'Topics')


@section('content')
    <div class="card shadow-sm">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class="bg-dark text-white">
                    <tr>
                        <th class="text-center">#</th>
                        <th>Title</th>
                        <th class="text-center">Topics</th>
                        <th>Created at</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($data as $item)
                    <tr>
                        <td class="text-center">{{++$loop->index}}</td>
                        <td>{!! $item['name'] !!}</td>
                        <td class="text-center">{{ sizeof($item['topic']) }}</td>
                        <td>{{ date('j F, Y', strtotime($item['created_at'])) }}</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="p-3 pb-5">
                            <table class="table m-0 mb-5 table-hover border">
                                <thead class="bg-light">
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Title</th>
                                        <th class="text-center">Tickets</th>
                                        <th>Created at</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($item['topic'] as $topic)
                                        <tr>
                                            <td width="40"><div class="text-center">&raquo;</div></td>
                                            <td><a href="/tickets/{{$topic['id']}}">{!! $topic['name'] !!}</a></td>
                                            <td class="text-center">{!! sizeof($topic['ticket']) !!}</td>
                                            <td>{{ date('j F, Y', strtotime($topic['created_at'])) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
