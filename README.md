## Khate Roshan Practice
My name is Vahid Kaargar and this project has been developed for interviews with Khate Roshan.

## Requirement
* Laravel 8.x
* PHP 7.3 >=

## Install
Via Composer
``` bash
$ composer install
```

When packages installed, create an empty mysql database and put info to .env file

Then run this:
``` bash
$ php artisan migrate
$ php artisan db:seed
```

You can now live the project on server
``` bash
$ php artisan serve
```